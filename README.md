# DevSecOps Practical Project Specification

## Introduction

The purpose of this document is to outline the project specification of the DevOps practical project you will be working on for the next part of your training.
This project will involve concepts from previous training modules, including:

- Secure Continuous Integration
- Secure Continuous Deployment

## Overview

You will be working with this externally developed application.

You are expected to create a section of a pipeline that creates a .jar artefact.

## Requirements

You will need to work with a security expert on this project.
Your colleague will need access to your section of the pipeline.
They will be able to offer solutions to any security risks so they need visual aids to help them find solutions to the risks but it your responsibility to implement any solutions.

When creating the artefact you need to follow all the best security practices that you have learnt.

Here are some steps to include in your pipeline to help follow best security practices.

- Unit Testing
- SAST and DAST Testing e.g. Sonarqube, ZAP
- Make use of a secrets manager e.g. AWS Secrets Manager
- Scan the artefact for vulnerabilities e.g. OWASP Dependency Check
- Consider using RBAC

## Application

This app makes use of an environment variable to keep the secret key secure.

```bash
export JWT_SECRET_KEY=[YOUR SECRET KEY]
```

### Test

To run the unit tests of the app you need to run the command.

```bash
mvn test
```

### Compile

To compile the code into a .jar file you need to run the following command.

```bash
mvn clean package
```

